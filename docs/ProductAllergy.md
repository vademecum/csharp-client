# Vdmcworld.Client.Model.ProductAllergy
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Product** | [**Product**](Product.md) |  | [optional] 
**Allergies** | [**List&lt;Allergy&gt;**](Allergy.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

