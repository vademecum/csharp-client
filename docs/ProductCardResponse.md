# Vdmcworld.Client.Model.ProductCardResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Success** | **bool?** |  | [optional] 
**Data** | [**ProductCard**](ProductCard.md) |  | [optional] 
**Error** | [**ErrorDetails**](ErrorDetails.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

