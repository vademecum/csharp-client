# Vdmcworld.Client.Model.AtcIndex
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**Tree** | [**List&lt;Atc&gt;**](Atc.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

