# Vdmcworld.Client.Model.ProductCardSpecialInformation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PediatricStatus** | [**Status**](Status.md) |  | [optional] 
**EmergencyPediatricStatus** | [**Status**](Status.md) |  | [optional] 
**LightProtectionStatus** | [**Status**](Status.md) |  | [optional] 
**MoistureProtectionStatus** | [**Status**](Status.md) |  | [optional] 
**ColdChainStatus** | [**Status**](Status.md) |  | [optional] 
**CytotoxicStatus** | [**Status**](Status.md) |  | [optional] 
**AdditionalMonitoringStatus** | [**Status**](Status.md) |  | [optional] 
**OrderedDistributionStatus** | [**Status**](Status.md) |  | [optional] 
**MandatoryChemoDrugStatus** | [**Status**](Status.md) |  | [optional] 
**DailyTreatmentStatus** | [**Status**](Status.md) |  | [optional] 
**IndicationCompatibilityStatus** | [**Status**](Status.md) |  | [optional] 
**OffLabelUsageStatus** | [**Status**](Status.md) |  | [optional] 
**HospitalProductStatus** | [**Status**](Status.md) |  | [optional] 
**DualPricedProductStatus** | [**Status**](Status.md) |  | [optional] 
**CurrentExchangeRateProductStatus** | [**Status**](Status.md) |  | [optional] 
**OrphanDrugStatus** | [**Status**](Status.md) |  | [optional] 
**PatientApprovalFormStatus** | [**Status**](Status.md) |  | [optional] 
**DrugSafetyMonitoringFormStatus** | [**Status**](Status.md) |  | [optional] 
**CeCertificationStatus** | [**Status**](Status.md) |  | [optional] 
**HighRiskStatus** | [**Status**](Status.md) |  | [optional] 
**FreeMarketPriceStatus** | [**Status**](Status.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

