# Vdmcworld.Client.Model.ProductCardCard
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NfcCode** | [**NfcCode**](NfcCode.md) |  | [optional] 
**PrescriptionType** | [**PrescriptionType**](PrescriptionType.md) |  | [optional] 
**EquivalentGroups** | **List&lt;string&gt;** |  | [optional] 
**ReimbursementStatus** | [**Status**](Status.md) |  | [optional] 
**SpecialInformation** | [**ProductCardSpecialInformation**](ProductCardSpecialInformation.md) |  | [optional] 
**SgkEquivalentCodes** | **List&lt;string&gt;** |  | [optional] 
**SgkPriceReferenceCodes** | **List&lt;string&gt;** |  | [optional] 
**Company** | [**Company**](Company.md) |  | [optional] 
**PublicNumber** | **string** |  | [optional] 
**AtcIndices** | [**List&lt;AtcIndex&gt;**](AtcIndex.md) |  | [optional] 
**Images** | **List&lt;System.IO.Stream&gt;** |  | [optional] 
**ShelfLife** | **string** |  | [optional] 
**Notice** | **string** |  | [optional] [default to "null"]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

