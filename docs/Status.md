# Vdmcworld.Client.Model.Status
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_Status** | **bool?** |  | [optional] 
**Description** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

