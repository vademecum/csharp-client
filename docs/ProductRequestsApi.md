# Vdmcworld.Client.Api.ProductRequestsApi

All URIs are relative to *https://api-test.vademecumonline.com.tr/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetProductAllergies**](ProductRequestsApi.md#getproductallergies) | **GET** /product-allergy/{productId} | Returns product allergies
[**GetProductCard**](ProductRequestsApi.md#getproductcard) | **GET** /product-card/{productId} | Returns a product card
[**GetProductCautionCard**](ProductRequestsApi.md#getproductcautioncard) | **GET** /product-caution-card/{productId} | Returns a product caution card


<a name="getproductallergies"></a>
# **GetProductAllergies**
> ProductAllergy GetProductAllergies (long? productId)

Returns product allergies

### Example
```csharp
using System;
using System.Diagnostics;
using Vdmcworld.Client.Api;
using Vdmcworld.Client.Client;
using Vdmcworld.Client.Model;

namespace Example
{
    public class GetProductAllergiesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("authorization", "Bearer");

            var apiInstance = new ProductRequestsApi();
            var productId = 789;  // long? | ID of product

            try
            {
                // Returns product allergies
                ProductAllergy result = apiInstance.GetProductAllergies(productId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProductRequestsApi.GetProductAllergies: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **long?**| ID of product | 

### Return type

[**ProductAllergy**](ProductAllergy.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getproductcard"></a>
# **GetProductCard**
> ProductCardResponse GetProductCard (long? productId)

Returns a product card

### Example
```csharp
using System;
using System.Diagnostics;
using Vdmcworld.Client.Api;
using Vdmcworld.Client.Client;
using Vdmcworld.Client.Model;

namespace Example
{
    public class GetProductCardExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("authorization", "Bearer");

            var apiInstance = new ProductRequestsApi();
            var productId = 789;  // long? | ID of product

            try
            {
                // Returns a product card
                ProductCardResponse result = apiInstance.GetProductCard(productId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProductRequestsApi.GetProductCard: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **long?**| ID of product | 

### Return type

[**ProductCardResponse**](ProductCardResponse.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getproductcautioncard"></a>
# **GetProductCautionCard**
> ProductCardResponse GetProductCautionCard (long? productId)

Returns a product caution card

### Example
```csharp
using System;
using System.Diagnostics;
using Vdmcworld.Client.Api;
using Vdmcworld.Client.Client;
using Vdmcworld.Client.Model;

namespace Example
{
    public class GetProductCautionCardExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("authorization", "Bearer");

            var apiInstance = new ProductRequestsApi();
            var productId = 789;  // long? | ID of product

            try
            {
                // Returns a product caution card
                ProductCardResponse result = apiInstance.GetProductCautionCard(productId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProductRequestsApi.GetProductCautionCard: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **long?**| ID of product | 

### Return type

[**ProductCardResponse**](ProductCardResponse.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

