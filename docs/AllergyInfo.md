# Vdmcworld.Client.Model.AllergyInfo
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Info** | **string** |  | [optional] 
**Desensitization** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

