# Vdmcworld.Client.Model.AuthResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Success** | **bool?** |  | [optional] 
**Data** | [**AuthResponseData**](AuthResponseData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

