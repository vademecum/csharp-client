# Vdmcworld.Client.Api.AuthenticationApi

All URIs are relative to *https://api-test.vademecumonline.com.tr/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Auth**](AuthenticationApi.md#auth) | **GET** /auth-token | Returns authentication tokens to be used.


<a name="auth"></a>
# **Auth**
> AuthResponse Auth ()

Returns authentication tokens to be used.

Makes a token request to the API and returns the tokens that can used in the other requests. To use the token field in the other requests, append a '__:__' to the provided token value, encode it with base64 and set the API token as __'*Basic base64ed_token_string_with_semicolon*__'

### Example
```csharp
using System;
using System.Diagnostics;
using Vdmcworld.Client.Api;
using Vdmcworld.Client.Client;
using Vdmcworld.Client.Model;

namespace Example
{
    public class AuthExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new AuthenticationApi();

            try
            {
                // Returns authentication tokens to be used.
                AuthResponse result = apiInstance.Auth();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AuthenticationApi.Auth: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AuthResponse**](AuthResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

