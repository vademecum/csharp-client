# Vdmcworld.Client.Model.Company
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long?** |  | [optional] 
**Name** | **string** |  | [optional] 
**OfficialName** | **string** |  | [optional] 
**Website** | **string** |  | [optional] 
**Fax** | **string** |  | [optional] 
**Phone** | **string** |  | [optional] 
**Address** | **string** |  | [optional] 
**Email** | **string** |  | [optional] 
**City** | **string** |  | [optional] 
**District** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

