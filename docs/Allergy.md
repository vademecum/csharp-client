# Vdmcworld.Client.Model.Allergy
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Substance** | [**Substance**](Substance.md) |  | [optional] 
**SubstanceAllergyInfo** | [**AllergyInfo**](AllergyInfo.md) |  | [optional] 
**AllergicReactions** | [**List&lt;AllergicReaction&gt;**](AllergicReaction.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

