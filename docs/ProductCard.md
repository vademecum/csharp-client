# Vdmcworld.Client.Model.ProductCard
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Product** | [**Product**](Product.md) |  | [optional] 
**Card** | [**ProductCardCard**](ProductCardCard.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

