/* 
 * Vademecum Online API
 *
 * This is the Vademecum API documentation which will allow you to connect to API.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: birce@vademecumonline.com.tr
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Vdmcworld.Client.Api;
using Vdmcworld.Client.Model;
using Vdmcworld.Client.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Vdmcworld.Client.Test
{
    /// <summary>
    ///  Class for testing File
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class FileTests
    {
        // TODO uncomment below to declare an instance variable for File
        //private File instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of File
            //instance = new File();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of File
        /// </summary>
        [Test]
        public void FileInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" File
            //Assert.IsInstanceOfType<File> (instance, "variable 'instance' is a File");
        }


        /// <summary>
        /// Test the property 'Url'
        /// </summary>
        [Test]
        public void UrlTest()
        {
            // TODO unit test for the property 'Url'
        }

    }

}
