/* 
 * Vademecum Online API
 *
 * This is the Vademecum API documentation which will allow you to connect to API.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: birce@vademecumonline.com.tr
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = Vdmcworld.Client.Client.SwaggerDateConverter;

namespace Vdmcworld.Client.Model
{
    /// <summary>
    /// AllergyInfo
    /// </summary>
    [DataContract]
    public partial class AllergyInfo :  IEquatable<AllergyInfo>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AllergyInfo" /> class.
        /// </summary>
        /// <param name="Info">Info.</param>
        /// <param name="Desensitization">Desensitization.</param>
        public AllergyInfo(string Info = default(string), string Desensitization = default(string))
        {
            this.Info = Info;
            this.Desensitization = Desensitization;
        }
        
        /// <summary>
        /// Gets or Sets Info
        /// </summary>
        [DataMember(Name="info", EmitDefaultValue=false)]
        public string Info { get; set; }

        /// <summary>
        /// Gets or Sets Desensitization
        /// </summary>
        [DataMember(Name="desensitization", EmitDefaultValue=false)]
        public string Desensitization { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class AllergyInfo {\n");
            sb.Append("  Info: ").Append(Info).Append("\n");
            sb.Append("  Desensitization: ").Append(Desensitization).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as AllergyInfo);
        }

        /// <summary>
        /// Returns true if AllergyInfo instances are equal
        /// </summary>
        /// <param name="input">Instance of AllergyInfo to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(AllergyInfo input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Info == input.Info ||
                    (this.Info != null &&
                    this.Info.Equals(input.Info))
                ) && 
                (
                    this.Desensitization == input.Desensitization ||
                    (this.Desensitization != null &&
                    this.Desensitization.Equals(input.Desensitization))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Info != null)
                    hashCode = hashCode * 59 + this.Info.GetHashCode();
                if (this.Desensitization != null)
                    hashCode = hashCode * 59 + this.Desensitization.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
