/* 
 * Vademecum Online API
 *
 * This is the Vademecum API documentation which will allow you to connect to API.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: birce@vademecumonline.com.tr
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RestSharp;
using Vdmcworld.Client.Client;
using Vdmcworld.Client.Model;

namespace Vdmcworld.Client.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IAuthenticationApi : IApiAccessor
    {
        #region Synchronous Operations
        /// <summary>
        /// Returns authentication tokens to be used.
        /// </summary>
        /// <remarks>
        /// Makes a token request to the API and returns the tokens that can used in the other requests. To use the token field in the other requests, append a &#39;__:__&#39; to the provided token value, encode it with base64 and set the API token as __&#39;*Basic base64ed_token_string_with_semicolon*__&#39;
        /// </remarks>
        /// <exception cref="Vdmcworld.Client.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>AuthResponse</returns>
        AuthResponse Auth ();

        /// <summary>
        /// Returns authentication tokens to be used.
        /// </summary>
        /// <remarks>
        /// Makes a token request to the API and returns the tokens that can used in the other requests. To use the token field in the other requests, append a &#39;__:__&#39; to the provided token value, encode it with base64 and set the API token as __&#39;*Basic base64ed_token_string_with_semicolon*__&#39;
        /// </remarks>
        /// <exception cref="Vdmcworld.Client.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of AuthResponse</returns>
        ApiResponse<AuthResponse> AuthWithHttpInfo ();
        #endregion Synchronous Operations
        #region Asynchronous Operations
        /// <summary>
        /// Returns authentication tokens to be used.
        /// </summary>
        /// <remarks>
        /// Makes a token request to the API and returns the tokens that can used in the other requests. To use the token field in the other requests, append a &#39;__:__&#39; to the provided token value, encode it with base64 and set the API token as __&#39;*Basic base64ed_token_string_with_semicolon*__&#39;
        /// </remarks>
        /// <exception cref="Vdmcworld.Client.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of AuthResponse</returns>
        System.Threading.Tasks.Task<AuthResponse> AuthAsync ();

        /// <summary>
        /// Returns authentication tokens to be used.
        /// </summary>
        /// <remarks>
        /// Makes a token request to the API and returns the tokens that can used in the other requests. To use the token field in the other requests, append a &#39;__:__&#39; to the provided token value, encode it with base64 and set the API token as __&#39;*Basic base64ed_token_string_with_semicolon*__&#39;
        /// </remarks>
        /// <exception cref="Vdmcworld.Client.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (AuthResponse)</returns>
        System.Threading.Tasks.Task<ApiResponse<AuthResponse>> AuthAsyncWithHttpInfo ();
        #endregion Asynchronous Operations
    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class AuthenticationApi : IAuthenticationApi
    {
        private Vdmcworld.Client.Client.ExceptionFactory _exceptionFactory = (name, response) => null;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationApi"/> class.
        /// </summary>
        /// <returns></returns>
        public AuthenticationApi(String basePath)
        {
            this.Configuration = new Configuration { BasePath = basePath };

            ExceptionFactory = Vdmcworld.Client.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public AuthenticationApi(Configuration configuration = null)
        {
            if (configuration == null) // use the default one in Configuration
                this.Configuration = Configuration.Default;
            else
                this.Configuration = configuration;

            ExceptionFactory = Vdmcworld.Client.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <value>The base path</value>
        public String GetBasePath()
        {
            return this.Configuration.ApiClient.RestClient.BaseUrl.ToString();
        }

        /// <summary>
        /// Sets the base path of the API client.
        /// </summary>
        /// <value>The base path</value>
        [Obsolete("SetBasePath is deprecated, please do 'Configuration.ApiClient = new ApiClient(\"http://new-path\")' instead.")]
        public void SetBasePath(String basePath)
        {
            // do nothing
        }

        /// <summary>
        /// Gets or sets the configuration object
        /// </summary>
        /// <value>An instance of the Configuration</value>
        public Configuration Configuration {get; set;}

        /// <summary>
        /// Provides a factory method hook for the creation of exceptions.
        /// </summary>
        public Vdmcworld.Client.Client.ExceptionFactory ExceptionFactory
        {
            get
            {
                if (_exceptionFactory != null && _exceptionFactory.GetInvocationList().Length > 1)
                {
                    throw new InvalidOperationException("Multicast delegate for ExceptionFactory is unsupported.");
                }
                return _exceptionFactory;
            }
            set { _exceptionFactory = value; }
        }

        /// <summary>
        /// Gets the default header.
        /// </summary>
        /// <returns>Dictionary of HTTP header</returns>
        [Obsolete("DefaultHeader is deprecated, please use Configuration.DefaultHeader instead.")]
        public IDictionary<String, String> DefaultHeader()
        {
            return new ReadOnlyDictionary<string, string>(this.Configuration.DefaultHeader);
        }

        /// <summary>
        /// Add default header.
        /// </summary>
        /// <param name="key">Header field name.</param>
        /// <param name="value">Header field value.</param>
        /// <returns></returns>
        [Obsolete("AddDefaultHeader is deprecated, please use Configuration.AddDefaultHeader instead.")]
        public void AddDefaultHeader(string key, string value)
        {
            this.Configuration.AddDefaultHeader(key, value);
        }

        /// <summary>
        /// Returns authentication tokens to be used. Makes a token request to the API and returns the tokens that can used in the other requests. To use the token field in the other requests, append a &#39;__:__&#39; to the provided token value, encode it with base64 and set the API token as __&#39;*Basic base64ed_token_string_with_semicolon*__&#39;
        /// </summary>
        /// <exception cref="Vdmcworld.Client.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>AuthResponse</returns>
        public AuthResponse Auth ()
        {
             ApiResponse<AuthResponse> localVarResponse = AuthWithHttpInfo();
             return localVarResponse.Data;
        }

        /// <summary>
        /// Returns authentication tokens to be used. Makes a token request to the API and returns the tokens that can used in the other requests. To use the token field in the other requests, append a &#39;__:__&#39; to the provided token value, encode it with base64 and set the API token as __&#39;*Basic base64ed_token_string_with_semicolon*__&#39;
        /// </summary>
        /// <exception cref="Vdmcworld.Client.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of AuthResponse</returns>
        public ApiResponse< AuthResponse > AuthWithHttpInfo ()
        {

            var localVarPath = "/auth-token";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new List<KeyValuePair<String, String>>();
            var localVarHeaderParams = new Dictionary<String, String>(Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
            };
            String localVarHttpContentType = Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/json"
            };
            String localVarHttpHeaderAccept = Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);


            // authentication (basicAuth) required
            // http basic authentication required
            if (!String.IsNullOrEmpty(Configuration.Username) || !String.IsNullOrEmpty(Configuration.Password))
            {
                localVarHeaderParams["Authorization"] = "Basic " + ApiClient.Base64Encode(Configuration.Username + ":" + Configuration.Password);
            }

            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) Configuration.ApiClient.CallApi(localVarPath,
                Method.GET, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("Auth", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<AuthResponse>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                (AuthResponse) Configuration.ApiClient.Deserialize(localVarResponse, typeof(AuthResponse)));
        }

        /// <summary>
        /// Returns authentication tokens to be used. Makes a token request to the API and returns the tokens that can used in the other requests. To use the token field in the other requests, append a &#39;__:__&#39; to the provided token value, encode it with base64 and set the API token as __&#39;*Basic base64ed_token_string_with_semicolon*__&#39;
        /// </summary>
        /// <exception cref="Vdmcworld.Client.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of AuthResponse</returns>
        public async System.Threading.Tasks.Task<AuthResponse> AuthAsync ()
        {
             ApiResponse<AuthResponse> localVarResponse = await AuthAsyncWithHttpInfo();
             return localVarResponse.Data;

        }

        /// <summary>
        /// Returns authentication tokens to be used. Makes a token request to the API and returns the tokens that can used in the other requests. To use the token field in the other requests, append a &#39;__:__&#39; to the provided token value, encode it with base64 and set the API token as __&#39;*Basic base64ed_token_string_with_semicolon*__&#39;
        /// </summary>
        /// <exception cref="Vdmcworld.Client.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (AuthResponse)</returns>
        public async System.Threading.Tasks.Task<ApiResponse<AuthResponse>> AuthAsyncWithHttpInfo ()
        {

            var localVarPath = "/auth-token";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new List<KeyValuePair<String, String>>();
            var localVarHeaderParams = new Dictionary<String, String>(Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
            };
            String localVarHttpContentType = Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/json"
            };
            String localVarHttpHeaderAccept = Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);


            // authentication (basicAuth) required
            // http basic authentication required
            if (!String.IsNullOrEmpty(Configuration.Username) || !String.IsNullOrEmpty(Configuration.Password))
            {
                localVarHeaderParams["Authorization"] = "Basic " + ApiClient.Base64Encode(Configuration.Username + ":" + Configuration.Password);
            }

            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) await Configuration.ApiClient.CallApiAsync(localVarPath,
                Method.GET, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("Auth", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<AuthResponse>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                (AuthResponse) Configuration.ApiClient.Deserialize(localVarResponse, typeof(AuthResponse)));
        }

    }
}
